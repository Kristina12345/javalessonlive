package jtm.activity05;

import jtm.activity04.Transport;

import java.util.Locale;

import jtm.activity04.Road;

public class Vehicle extends Transport {
	protected int wheels;
public Vehicle(String id, float consumption, int tankSize,  int wheels) {
		super(id, consumption, tankSize);
		this.wheels = wheels;
	}

@Override
public String move(Road road) {
	
	if(road.getClass() != Road.class) {
		return  "Cannot drive on "+ road.toString();
	}else{
		float necessaryFuel = road.getDistance() * getConsumption() / 100;
		if (necessaryFuel >= this.getFuelInTank())
			return "Cannot move on " + road + ". Necessary fuel:" + String.format(Locale.US, "%.2f", necessaryFuel)
					+ "l, fuel in tank:" + String.format(Locale.US, "%.2f", this.getFuelInTank()) + "l";
		else {
			this.setFuelInTank(getFuelInTank() - necessaryFuel);
			return this.getId() + " Vehicle is driving on " + road.toString() +" with "+ this.wheels + " wheels";
		}		
	}
}
}
