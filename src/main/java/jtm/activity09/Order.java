package jtm.activity09;

import java.util.Objects;

/*- TODO #1
  * Hint! Use generic type of comparable items in form: Comparable<Order>
 * 
 * TODO #2 Override/implement necessary methods for Order class:
 * - public Order(String orderer, String itemName, Integer count) — constructor of the Order
 * - public int compareTo(Order order) — comparison implementation according to logic described below
 * - public boolean equals(Object object) — check equality of orders
 * - public int hashCode() — to be able to handle it in some hash... collection 
 * - public String toString() — string in following form: "ItemName: OrdererName: Count"
 * 
 * Hints:
 * 1. When comparing orders, compare their values in following order:
 *    - Item name
 *    - Customer name
 *    - Count of items
 * If item or customer is closer to start of alphabet, it is considered "smaller"
 * 
 * 2. When implementing .equals() method, rely on compareTo() method, as for sane design
 * .equals() == true, if compareTo() == 0 (and vice versa).
 * 
 * 3. Also Ensure that .hashCode() is the same, if .equals() == true for two orders.
 * 
 */

public class Order implements Comparable<Order>
{
	String customer; // Name of the customer
	String name; // Name of the requested item
	int count; // Count of the requested items
	
	
	public Order(String orderer, String itemName, Integer count) {
		this.customer = orderer;
		this.name = itemName;
		this.count = count;
		
	}
	

	
	

	public Order(String orderer, int i, String string) {
		// TODO Auto-generated constructor stub
	}



	@Override
	public int compareTo(Order o) {
		int tempOrdered = this.customer.compareTo(o.customer);
		if (tempOrdered  < 0) {  
			tempOrdered  = -1;
		    //a is smaller
		}
		else if (tempOrdered> 0) {
			tempOrdered = 1;
		    //a is larger 
		}
		else {  
			tempOrdered = 0;
		    //a is equal to b
		}
		
		int tempItemName = this.name.compareTo(o.name);  
if (tempOrdered == 0) {
		if (tempItemName < 0) {  
			tempItemName = -1;
		    //a is smaller
		}
		else if (tempItemName> 0) {
			tempItemName = 1;
		    //a is larger 
		}
		else {  
			tempItemName = 0;
		    //a is equal to b
		}
}
//int tempCount= this.count=o.count;  
Integer obj1 = this.count;
Integer obj2 = o.count;
int tempCount= obj1.compareTo(obj2);
if ((tempOrdered == 0)&&(tempItemName==0)) {
	if (tempCount < 0) {  
		tempCount = -1;
	    //a is smaller
	}
	else if (tempCount> 0) {
		tempCount = 1;
	    //a is larger 
	}
	else {  
		tempCount= 0;
	    //a is equal to b
	}
}
if(tempOrdered!=0) {
	return tempOrdered;
}else if(tempItemName!=0){
	return tempItemName;
}else {return tempCount;
}
		
	}
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(!(obj instanceof Order)) {
			return false;
		}
		Order other = (Order)obj;
		
		return count == other.count&&Objects.equals(customer, other.customer)&&Objects.equals(name, other.name);
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return customer.hashCode()* name.hashCode()*(int)count;
	}

	@Override
	public String toString() {
		
		return "ItemName: "+this.name+"OrdererName: "+this.customer+"Count: "+this.count;
	}
}

