package jtm.activity09;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;


/*- TODO #2
 * Implement Iterator interface with Orders class
 * Hint! Use generic type argument of iterateable items in form: Iterator<Order>
 * 
 * TODO #3 Override/implement public methods for Orders class:
 * - Orders()                — create new empty Orders
 * - add(Order item)            — add passed order to the Orders
 * - List<Order> getItemsList() — List of all customer orders
 * - Set<Order> getItemsSet()   — calculated Set of Orders from list (look at description below)
 * - sort()                     — sort list of orders according to the sorting rules
 * - boolean hasNext()          — check is there next Order in Orders
 * - Order next()               — get next Order from Orders, throw NoSuchElementException if can't
 * - remove()                   — remove current Order (order got by previous next()) from list, throw IllegalStateException if can't
 * - String toString()          — show list of Orders as a String
 * 
 * Hints:
 * 1. To convert Orders to String, reuse .toString() method of List.toString()
 * 2. Use built in List.sort() method to sort list of orders
 * 
 * TODO #4
 * When implementing getItemsSet() method, join all requests for the same item from different customers
 * in following way: if there are two requests:
 *  - ItemN: Customer1: 3
 *  - ItemN: Customer2: 1
 *  Set of orders should be:
 *  - ItemN: Customer1,Customer2: 4
 */
/*-
 * TODO #1
 * Create data structure to hold:
 *   1. some kind of collection of Orders (e.g. some List)
 *   2. index to the current order for iterations through the Orders in Orders
 *   Hints:
 *   1. you can use your own implementation or rely on .iterator() of the List
 *   2. when constructing list of orders, set number of current order to -1
 *      (which is usual approach when working with iterateable collections).
 */
public class Orders implements Iterator<Order>{

	private List<Order>orders;//this list stores the collection
	private int currentOrder;// current position;
	//these parameters are wrapped inside, nobody from outside should access them, thet's why they are private
	
	public Orders(){
		this.orders =new ArrayList<Order>();
		this.currentOrder = -1; // negative index means that we have no elements in the collection
	}
	
	@Override
	public boolean hasNext() { // we are at the current position at the moment, and we want to check if there is something on the next position
		if(this.orders.size()>0 && currentOrder<this.orders.size())//if this condition is true, it means, that it has the next one
			return true;
		else
		return false;
	}
	@Override
	public Order next() {// we want to check if there is next element; we ask for the next order
		if(this.hasNext()) {
			this.currentOrder++;
		return this.orders.get(this.currentOrder);
		}
		else
			throw new NoSuchElementException();
	}
	public void add(Order item) { //pass elements to the collection
	this.orders.add(item);
	}
	public List<Order> getItemsList(){
		return this.orders;
	}
	public void sort() {
		Collections.sort(this.orders);
	}
	@Override
	public void remove() {
		if(this.currentOrder>=0) {
			this.orders.remove(this.currentOrder);
			this.currentOrder--;
		}else {
			throw new IllegalStateException();
		}
	}

	@Override
	public String toString() {
		return this.orders.toString();
	}
	public Set<Order> getItemsSet(){
		Set<Order>orderSet = new TreeSet<Order>(); // set contains only unique elements
		Order prevOrder = null;
		this.sort(); // to have orders in the correct sequence
		for(Order order:this.orders) {
			if(prevOrder==null) {
				prevOrder = order; 
				continue;
			}
			if(order.name.equals(prevOrder.name)) {
				prevOrder.count+=order.count; // add the current count to the previous one
				prevOrder.customer+=", "+order.customer;
				
			}
			else {
				orderSet.add(prevOrder);
				prevOrder = order;
			}
		}
		if(prevOrder!=null)
			orderSet.add(prevOrder);
		return orderSet;
		
	}
	
}
	/**int currentOrder = 0;
	List<Order> orderList;
	Set<Order>orderSet = new HashSet<>();

	
public Orders() {
	currentOrder = -1;
	orderList = new ArrayList<>();
}
public void add(Order item) {
	this.orderList.add(item);
}
	
public List<Order> getItemsList(){
	return orderList;
	
}
List<String>customers = new ArrayList<>();
Set<Order>resultOrders = new HashSet<>();
public Set<Order> getItemsSet( ) {
	int i = 0;
	Order current = null;
	int totalAmount = 0;
	
	List<Order>orders = new ArrayList<>();
	sort();
	for(Order order:orderList) {
		i++;
		if(current==null) {
			current =order;	
		}
		if(current.name == order.name) {
			totalAmount+=order.count;
			if(!( customers.contains(order.customer))) {
				customers.add(order.customer);

			}else {
				current = (Order) orderList;
				resultOrders.add(new Order(current.name, order.count = totalAmount, order.customer = customers.toString()));
			//	resultOrders ();
				totalAmount = 0;
				customers.clear();
				totalAmount =current.count;
				customers.addAll((Collection<? extends String>) current);
			}
			
		
		if(i==orderList.size()) {
			resultOrders.add(new Order(current.name, order.count = totalAmount, order.customer = customers.toString()));
			//resultOrders ();
		}}
	}
	
	
	return  resultOrders;



}

public void sort() {
	Collections.sort(orderList);
}
StringBuilder sb = new StringBuilder();
	
	@Override
	public boolean hasNext() {
		
	return false;
	        
	            
	}
	@Override
	public Order next(){
		if (!hasNext()) throw new NoSuchElementException();
		// List<Order> result = orderList;
		 orderList = null;
		// boolean canRemove = true;
		 return (Order) orderList;
		
	
	}
	@Override
	public void remove() {
		if (!hasNext()) throw new NoSuchElementException();
		// TODO Auto-generated method stub
		Iterator.super.remove();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return orderList.toString();
	}*/

	

