package jtm.extra04;


import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class StringTokenizerExercise {
	
	private BufferedReader input;
	
	public static void main(String [] args) {
		
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {String filepath = reader.readLine();
		String delim = reader.readLine();
		
		StringTokenizerExercise obj = new StringTokenizerExercise();
		List<Student> list = obj.createFromFile(filepath, delim);
		System.out.println(list.toString());
		}
		catch(IOException e){
			System.out.println("Error occured!");
			
		}
	}
	public String[] splitString(String text, String delimiter) {
			String[] list = text.split(delimiter);
			for (String a: list)
	            System.out.println(a);
			
			// TODO # 1 Split passed text by given delimiter and return array with
			// split strings.
			// HINT: Use System.out.println to better understand split method's
			// functionality.
			return list;
		}

		public List<String> tokenizeString(String text, String delimiter) {
			// TODO # 2 Tokenize passed text by given delimiter and return list with
			// tokenized strings.
			List<String> list = new ArrayList<>();
		    StringTokenizer tokenizer = new StringTokenizer(text, delimiter);//Notice how we're breaking the String into the list of tokens based on delimiter �,�.
			 while (tokenizer.hasMoreElements()) { // Then in the loop, using tokens.add() method; we are adding each token into the ArrayList.
			        list.add(tokenizer.nextToken());
			    }
			return list;
		}



	public List<Student> createFromFile(String filepath, String delimiter) {
		
		// TODO # 3 Implement method which reads data from file and creates
		// Student objects with that data. Each line from file contains data for
		// 1 Student object.
		// Add students to list and return the list. Assume that all passed data
		// and
		// files are correct and in proper form.
		// Advice: Explore StringTokenizer or String split options.
		
		File students = new File(filepath);
		List<Student> list = new ArrayList<Student>();
		
		//BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try{ input  =new BufferedReader(new FileReader(students));

		    String lineSTR = "\n";
		    boolean firstLine = true;
		    String[]columns = new String[4];
		    while((lineSTR = input.readLine())!=null) {
		       StringTokenizer line = new StringTokenizer(lineSTR, delimiter);
		       
		       if(firstLine == true) {
		    	   columns[0] = (String)line.nextElement();
		    	   columns[1] = (String)line.nextElement();
		    	   columns[2] = (String)line.nextElement();
		    	  columns[3] = (String)line.nextElement();
		    	   firstLine = false;
		    	   continue;
		       }
		       Student stu = new Student();
		       setValue(columns[0], line, stu);
		       setValue(columns[1], line, stu);
		       setValue(columns[2], line, stu);
		     setValue(columns[3], line, stu);
		       list.add(stu);
		    }
		   
		}
		catch(IOException e) {
			System.out.println("Error occured!");
		}

		return list;
	}
	private void setValue(String string, StringTokenizer line, Student stu) {
		switch (string) {
		case "ID":
			stu.setID(Integer.valueOf((String)line.nextElement()));
			break;
		case "FirstName":
			stu.setFirstName((String)line.nextElement());
			break;
		case "LastName":
			stu.setLastName((String)line.nextElement());
			break;
		case "PhoneNumber":
			stu.setPhoneNumber(Integer.valueOf((String)line.nextElement()));
			break;
		}
		
	}
}
	
	


