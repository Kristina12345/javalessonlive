package jtm.extra04;

public class Student {
	private int ID;
	private String firstName;
	private String lastName;
	private int phoneNumber;

	public Student() {
		this(0, " ", " ", 0);
	};

	public Student(int ID, String firstName, String lastName, int phoneNumber) {
		// TODO #1 Create new student, assign him ID, first name, last name and
		// phone number from passed values
		//Student mystudent= new Student();
		this.ID = ID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
	}
	/**public static void main (String [] args) {
		Student mystudent= new Student();
		System.out.println(mystudent);
	}*/

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
